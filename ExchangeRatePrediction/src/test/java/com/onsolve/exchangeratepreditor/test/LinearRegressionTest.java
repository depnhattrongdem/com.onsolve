package com.onsolve.exchangeratepreditor.test;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import com.onsolve.exchangeratepreditor.Configuration;
import com.onsolve.exchangeratepreditor.ExchangeRatePrediction;
import com.onsolve.exchangeratepreditor.ExchangeRateSample;
import com.onsolve.exchangeratepreditor.Utilities;
import com.onsolve.linearregression.LinearRegression;
import com.openexchangerate.service.Currency;

import junit.framework.TestCase;

public class LinearRegressionTest extends TestCase {

	List<ExchangeRateSample> exchangeRateSampleList = new ArrayList<ExchangeRateSample>();
	LinearRegression linearRegression = null;
	Configuration config = Configuration.getInstance();

	protected void setUp() {
		this.exchangeRateSampleList = loadSampleDataFile();
	}

	public void testLinearRegression() {
		LinearRegression linearRegression = new LinearRegression(this.convert(exchangeRateSampleList));
		System.out.println("N(Number of values or elements): " + linearRegression.getN());
		System.out.println("ΣX(Sum of First Scores): " + linearRegression.getSigmaX());
		System.out.println("ΣY(Sum of Second Scores): " + linearRegression.getSigmaY());
		System.out.println("ΣXY(Sum of the product of first and Second Scores): " + linearRegression.getSigmaXY());		
		System.out.println("ΣX2(Sum of square First Scores): " + linearRegression.getSigmaSquareX());		
		System.out.println("b(The slope formula): " + linearRegression.calculateSlopeFormular());
		System.out.println("a(The intercept formula): " + linearRegression.calculateInterceptFormular());
		System.out.println("x(The input variable): " + Utilities.toStringDate(config.getPredictionDate()));
		System.out.println("y(The regression equation formula): " + linearRegression.calculateRegressionEquation(config.getPredictionDate().getMonthValue()));
	}

	public void testExchangeRatePrediction() {
		ExchangeRatePrediction preditor = new ExchangeRatePrediction(exchangeRateSampleList);
		LocalDate date = config.getPredictionDate();
		BigDecimal preditionValue = preditor.exchangeRatePrediction(date);
		System.out.println(String.format(("The predicted currency exchange from %s to %s for %s is %s"), Currency.USD,
				Currency.TRY, Utilities.toStringDate(date), preditionValue));
	}
	
	public Dictionary<Integer, BigDecimal> convert(List<ExchangeRateSample> sampling) {
		Dictionary<Integer, BigDecimal> dictionary = new Hashtable<Integer, BigDecimal>();

		for (ExchangeRateSample exchangeRateSample : sampling) {
			dictionary.put(exchangeRateSample.getDate().getMonthValue(), exchangeRateSample.getRate());
		}

		return dictionary;
	}

	private List<ExchangeRateSample> loadSampleDataFile() {
		List<ExchangeRateSample> list = new ArrayList<ExchangeRateSample>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("exchange_rate_sample_data.txt"));
			String line = br.readLine();

			while (line != null) {
				String[] arr = line.split("\t");
				if (arr.length >= 2) {
					String strDate = arr[0];
					String strRate = arr[1];
					LocalDate localDate = Utilities.toLocaldate(strDate);
					BigDecimal rate = new BigDecimal(strRate);
					ExchangeRateSample sample = new ExchangeRateSample(rate, localDate);
					list.add(sample);
				}
				line = br.readLine();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return list;
	}
	
	public void test(){
		
	}
}
