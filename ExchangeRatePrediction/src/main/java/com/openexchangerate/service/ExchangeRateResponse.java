package com.openexchangerate.service;

public class ExchangeRateResponse {
	private String disclaimer;
	private String license;
	private Long timestamp;
	private String base;
	private ExchangeRate rates;

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public ExchangeRate getRates() {
		return rates;
	}

	public void setRates(ExchangeRate rates) {
		this.rates = rates;
	}

}
