package com.openexchangerate.service;

import java.math.BigDecimal;

public class ExchangeRate {
	private BigDecimal USD;
	private BigDecimal TRY;
	private BigDecimal AED;
	private BigDecimal AFN;
	private BigDecimal ALL;
	private BigDecimal AMD;
	private BigDecimal ANG;
	private BigDecimal AOA;
	private BigDecimal ARS;
	private BigDecimal AUD;
	private BigDecimal AWG;
	private BigDecimal AZN;
	private BigDecimal BAM;
	private BigDecimal BBD;
	private BigDecimal BDT;
	private BigDecimal BGN;
	private BigDecimal BHD;
	private BigDecimal BIF;
	private BigDecimal BMD;
	private BigDecimal BND;
	private BigDecimal BOB;
	private BigDecimal BRL;
	private BigDecimal BSD;

	public BigDecimal getRateOf(Currency currency) {
		switch (currency) {
		case AED:
			return AED;
		case USD:
			return USD;
		case TRY:
			return TRY;
		case AFN:
			return AFN;
		case ALL:
			return ALL;
		case AMD:
			return AMD;
		case ANG:
			return ANG;
		case AOA:
			return AOA;
		case ARS:
			return ARS;
		case AUD:
			return AUD;
		case AWG:
			return AWG;
		case AZN:
		 	return AZN;
		case BAM:
			return BAM;
		case BBD:
			return BBD;
		case BDT:
			return BDT;
		case BGN:
			return BGN;
		case BHD:
			return BHD;
		case BIF:
			return BIF;
		case BMD:
			return BMD;
		case BND:
			return BND;
		case BOB:
			return BOB;
		case BRL:
			return BRL;
		case BSD:
			return BSD;
		default:
			return new BigDecimal(0);
		}		
	}
}
