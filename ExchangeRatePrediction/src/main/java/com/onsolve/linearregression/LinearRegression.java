package com.onsolve.linearregression;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;

/**
 * Regression Formula: Regression Equation(y) = a + bx Slope(b) = (NΣXY -
 * (ΣX)(ΣY)) / (NΣX2 - (ΣX)2) Intercept(a) = (ΣY - b(ΣX)) / N
 *
 * Where: x and y are the variables. b = The slope of the regression line a =
 * The intercept point of the regression line and the y axis. N = Number of
 * values or elements X = First Score (the first sampling list) Y = Second Score
 * (the second sampling list) ΣXY = Sum of the product of first and Second
 * Scores ΣX = Sum of First Scores ΣY = Sum of Second Scores ΣX2 = Sum of square
 * First Scores
 * 
 * @author Thach
 */
public class LinearRegression {
	private List<Integer> X = new ArrayList<Integer>();
	private List<BigDecimal> Y = new ArrayList<BigDecimal>();
	private int N = 0;
	private int sigmaX;
	private BigDecimal sigmaY;
	private BigDecimal sigmaXY;
	private int sigmaSquareX;
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
	private static final int SCALE = 3;

	public LinearRegression(Dictionary<Integer, BigDecimal> sampling) {

		Enumeration<Integer> key = sampling.keys();
		while (key.hasMoreElements()) {
			this.X.add(key.nextElement());
		}

		Enumeration<BigDecimal> element = sampling.elements();
		while (element.hasMoreElements()) {
			this.Y.add(element.nextElement());
		}

		this.N = sampling.size();
		this.sigmaX = sumIntegerInList(X);
		this.sigmaY = sumBigDecimalInList(Y);
		this.sigmaXY = sumOfMultiply(X, Y, N);
		this.sigmaSquareX = sumOfSquare(X);
	}

	public BigDecimal calculateRegressionEquation(int firstCoreX) {
		int x = firstCoreX;
		BigDecimal a = calculateInterceptFormular();
		BigDecimal b = calculateSlopeFormular();

		BigDecimal y = a.add(b.multiply(new BigDecimal(x)));

		return y.setScale(SCALE, ROUNDING_MODE);
	}

	public BigDecimal calculateInterceptFormular() {
		BigDecimal result = new BigDecimal(0);

		BigDecimal b = calculateSlopeFormular();
		
		BigDecimal temp1 = b.multiply(new BigDecimal(sigmaX));
		BigDecimal temp2 = sigmaY.subtract(temp1);
		result = temp2.divide(new BigDecimal(N),ROUNDING_MODE);

		return result.setScale(SCALE, ROUNDING_MODE);
	}

	public BigDecimal calculateSlopeFormular() {
		BigDecimal result = new BigDecimal(0);

		BigDecimal temp1 = sigmaXY.multiply(new BigDecimal(N)).subtract(sigmaY.multiply(new BigDecimal(sigmaX)));
		BigDecimal temp2 = new BigDecimal(N * sigmaSquareX - sigmaX * sigmaX);
		result = temp1.divide(temp2, ROUNDING_MODE);

		return result.setScale(SCALE, ROUNDING_MODE);
	}

	private int sumOfSquare(List<Integer> list) {
		int sum = 0;

		for (int item : list) {
			int temp = item * item;
			sum += temp;
		}

		return sum;
	}

	private int sumIntegerInList(List<Integer> list) {
		int sum = 0;

		for (int item : list) {
			sum += item;
		}

		return sum;
	}

	private BigDecimal sumBigDecimalInList(List<BigDecimal> list) {
		BigDecimal sum = new BigDecimal(0);

		for (BigDecimal item : list) {
			sum = sum.add(item);
		}

		return sum.setScale(SCALE, ROUNDING_MODE);
	}

	private BigDecimal sumOfMultiply(List<Integer> list1, List<BigDecimal> list2, int size) {
		BigDecimal sum = new BigDecimal(0);

		for (int i = 0; i < N; i++) {
			BigDecimal temp = list2.get(i).multiply(new BigDecimal(list1.get(i)));
			sum = sum.add(temp);
		}

		return sum.setScale(SCALE, ROUNDING_MODE);
	}

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getSigmaX() {
		return sigmaX;
	}

	public void setSigmaX(int sigmaX) {
		this.sigmaX = sigmaX;
	}

	public BigDecimal getSigmaY() {
		return sigmaY;
	}

	public void setSigmaY(BigDecimal sigmaY) {
		this.sigmaY = sigmaY;
	}

	public BigDecimal getSigmaXY() {
		return sigmaXY;
	}

	public void setSigmaXY(BigDecimal sigmaXY) {
		this.sigmaXY = sigmaXY;
	}

	public int getSigmaSquareX() {
		return sigmaSquareX;
	}

	public void setSigmaSquareX(int sigmaSquareX) {
		this.sigmaSquareX = sigmaSquareX;
	}
	
	
}
