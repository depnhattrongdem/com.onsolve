package com.onsolve.exchangeratepreditor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import com.openexchangerate.service.Currency;
import com.openexchangerate.service.OpenExchangeRateService;

public class ExchangeRateSampleProviding {
	
	private final int THREAD_POOL_SIZE = 12;
	OpenExchangeRateService service = (OpenExchangeRateService) ExchangeRateServiceFactory
			.newExchangeRateService(OpenExchangeRateService.class.getName());
	ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(THREAD_POOL_SIZE);	
	
	public List<ExchangeRateSample> getSampleFromOpenExchangeProvider(Currency fromCurrency, Currency toCurrency) {

		List<ExchangeRateSample> sampling = new ArrayList<ExchangeRateSample>();
		List<LocalDate> dateList = this.buildDateList();
		
		System.out.println("Collecting the sampling from open exchange rate service ...");
		
		for (LocalDate date : dateList) {
						
			try {
				Future<ExchangeRateSample> response = executor.submit(new SampleProvidingTask(fromCurrency, toCurrency, date));				
				ExchangeRateSample sample = response.get();
				sampling.add(sample);
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		executor.shutdown();

		return sampling;
	}

	private List<LocalDate> buildDateList() {
		List<LocalDate> dateList = new ArrayList<LocalDate>();
		
		for(int i=0; i<12; i++){
			LocalDate date = LocalDate.of(2016, i+1, 15);
			dateList.add(date);
		}		
		
		return dateList;
	}

	class SampleProvidingTask implements Callable<ExchangeRateSample>{

		private Currency fromCurrency;
		private Currency toCurrency;	
		private LocalDate date;
		
		public SampleProvidingTask(Currency fromCurrency, Currency toCurrency, LocalDate date) {
			this.fromCurrency = fromCurrency;
			this.toCurrency = toCurrency;
			this.date = date;
		}
		
		public ExchangeRateSample call() throws Exception {
			
			ExchangeRateSample response = null;
			
			try {				
				response = service.getHistoricalExchangeRate(fromCurrency, toCurrency, date);				
				System.out.println(String.format("From %s to %s for %s is %s", fromCurrency.name(), toCurrency.name(),
						Utilities.toStringDate(date), response.getRate()));
			} catch (Exception e) {
				throw e;
			}
						
			return response;
		}
		
	}
}
