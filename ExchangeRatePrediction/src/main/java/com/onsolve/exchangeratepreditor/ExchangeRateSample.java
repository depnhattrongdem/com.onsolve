package com.onsolve.exchangeratepreditor;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExchangeRateSample {
	private BigDecimal rate;
	private LocalDate date;
	
	public ExchangeRateSample() {		
	}
	
	public ExchangeRateSample(BigDecimal rate, LocalDate date) {
		this.rate = rate;
		this.date = date;
	}
	
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}		
}
