package com.onsolve.exchangeratepreditor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Utilities {
	
	static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static String toStringDate(LocalDate date) {
		DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return date.format(sdf);
	}
	
	public static LocalDate toLocaldate(String date) throws Exception{
		try {
			LocalDate localDate = LocalDate.parse(date, dateFormatter);
			return localDate;
		} catch (Exception e) {
			throw e;
		}		
	}
}
