package com.onsolve.exchangeratepreditor;

import com.openexchangerate.service.OpenExchangeRateService;

public class ExchangeRateServiceFactory {
	
	public static ExchangeRateService newExchangeRateService(String className){
		if (className.equals(OpenExchangeRateService.class.getName())) {
			return new OpenExchangeRateService();			
		}
		return null;
	}
}
