package com.onsolve.exchangeratepreditor;

import java.time.LocalDate;
import com.openexchangerate.service.Currency;

public interface ExchangeRateService {
	public ExchangeRateSample getLatestExchangeRate(Currency fromCurrency, Currency toCurrency) throws Exception;
	public ExchangeRateSample getHistoricalExchangeRate(Currency baseCurrency, Currency toCurrency, LocalDate date) throws Exception;
}
