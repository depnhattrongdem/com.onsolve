package com.onsolve.exchangeratepreditor;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import com.openexchangerate.service.Currency;


public class Main {
		
	public static void main(String[] args) {
		
	    Currency fromCurrency = null;
		Currency toCurrency = null;
		boolean correct = true;
		Configuration config = Configuration.getInstance();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Exchange Rate Prediction Application");
		System.out.println(
				"Supported Currencies : {USD,TRY,AED,AFN,ALL,AMD,ANG,AOA,ARS,AUD,AWG,AZN,BAM,BBD,BDT,BGN,BHD,BIF,BMD,BND,BOB,BRL,BSD}");
		try {
			do {
				System.out.println("Please input the original Currency: ");
				String input = scanner.nextLine();
				input = input.toUpperCase();
				try {
					fromCurrency = Currency.valueOf(input);
					correct = true;
				} catch (Exception e) {	
					System.out.println("This currency not supported");
					correct = false;
				}					
			} while (!correct);

			do {
				System.out.println("Please input the destination Currency: ");
				String input = scanner.nextLine();
				input = input.toUpperCase();
				try {
					toCurrency = Currency.valueOf(input);
					correct = true;
				} catch (Exception e) {
					System.out.println("This currency not supported");
					correct = false;
				}								
			} while (!correct);

			ExchangeRateSampleProviding providing = new ExchangeRateSampleProviding();
			List<ExchangeRateSample> sampling = providing.getSampleFromOpenExchangeProvider(fromCurrency, toCurrency);					
			
			ExchangeRatePrediction preditor = new ExchangeRatePrediction(sampling);
			
			LocalDate date = config.getPredictionDate();
			if(date != null){
				BigDecimal preditionValue = preditor.exchangeRatePrediction(date);			
				System.out.println(String.format(("The predicted currency exchange from %s to %s for %s is %s"), fromCurrency, toCurrency, Utilities.toStringDate(date), preditionValue));
			}
			else{
				System.out.println(String.format(("The prediction date is null")));
			}

			
		} catch (Exception e) {
			System.out.println("The application has corrupted: " + e.getMessage());
		}finally {
			scanner.close();
		}
	}
}
