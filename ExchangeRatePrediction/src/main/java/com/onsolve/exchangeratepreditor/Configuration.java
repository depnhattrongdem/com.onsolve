package com.onsolve.exchangeratepreditor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Properties;

public class Configuration {
	final String filename = "configuration.properties";	
	private static Configuration instance = null;
	private LocalDate predictionDate = null;
	private String openExchangeRateAPIKey;

	private Configuration() {
		// TODO Auto-generated constructor stub
	}

	public static Configuration getInstance() {
		if(instance == null){
			instance = new Configuration();
			instance.loadConfiguration();			
		}
		return instance;
	}

	private void loadConfiguration() {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(filename);
			prop.load(input);

			openExchangeRateAPIKey = prop.getProperty("openExchangeRateKey");
			predictionDate = Utilities.toLocaldate(prop.getProperty("predictionDate"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public LocalDate getPredictionDate() {
		return predictionDate;
	}

	public void setPredictionDate(LocalDate predictionDate) {
		this.predictionDate = predictionDate;
	}

	public String getOpenExchangeRateAPIKey() {
		return openExchangeRateAPIKey;
	}

	public void setOpenExchangeRateAPIKey(String openExchangeRateAPIKey) {
		this.openExchangeRateAPIKey = openExchangeRateAPIKey;
	}
}
