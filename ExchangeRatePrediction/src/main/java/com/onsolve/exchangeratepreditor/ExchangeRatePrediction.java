package com.onsolve.exchangeratepreditor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import com.onsolve.linearregression.LinearRegression;

public class ExchangeRatePrediction {

	Dictionary<Integer, BigDecimal> sampling = new Hashtable<Integer, BigDecimal>();

	public ExchangeRatePrediction(List<ExchangeRateSample> sampling) {
		this.sampling = convert(sampling);
	}

	private Dictionary<Integer, BigDecimal> convert(List<ExchangeRateSample> sampling) {
		Dictionary<Integer, BigDecimal> dictionary = new Hashtable<Integer, BigDecimal>();

		for (ExchangeRateSample exchangeRateSample : sampling) {
			dictionary.put(exchangeRateSample.getDate().getMonthValue(), exchangeRateSample.getRate());
		}

		return dictionary;
	}

	public BigDecimal exchangeRatePrediction(LocalDate date) {
		BigDecimal result = new BigDecimal(0);

		LinearRegression linearReg = new LinearRegression(sampling);		
		result = linearReg.calculateRegressionEquation(date.getMonthValue());

		return result;
	}
}
